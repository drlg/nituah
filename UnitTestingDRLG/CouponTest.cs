﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using DRLG;

namespace UnitTestingDRLG
{
    [TestClass]
    public class CouponTest
    {

        mainData dal = mainData.getInstance();



        [TestMethod]
        public void TestMethod11()
        {
            User_account bmac = new User_account("Manager2", "1");
            BusinessManager bm = new BusinessManager(bmac, "Manager", "Manager", "054123456", "Manager@manage.com");
            dal.insertBusinessManager(bm);
            Business b = new Business("Rehovot", "5 star Spa", "Olympus", "SPA", bm);
            dal.insertBusiness(b);
            CouponInfo coupInf = new CouponInfo(b, 123457, 450, 300, new DateTime(2015, 06, 30), "package of 3 threatments for the amazing price of 100 each");
            String ans = dal.insertCouponInfo(coupInf);
            Assert.AreEqual(ans, "Coupon info added succsesfully", true, "test 11 failed");
        }

        [TestMethod]
        public void TestMethod12()
        {
            User_account bmac = new User_account("Manager1", "1");
            BusinessManager bm = new BusinessManager(bmac, "Manager", "Manager", "054123456", "Manager@manage.com");
            dal.insertBusinessManager(bm);
            Business b = new Business("Rehovot", "5 star Spa", "Olympus", "SPA", bm);
            dal.insertBusiness(b);
            CouponInfo coupInf = new CouponInfo(b, 123457, 450, 300, new DateTime(2015, 06, 30), "package of 3 threatments for the amazing price of 100 each");
            dal.insertCouponInfo(coupInf);
            Coupon coup = new Coupon("123456", 123457, coupInf);
            dal.insertCoupon(coup);
            Assert.AreEqual(dal.findCouponBySerial("123456").getSerial(),"123456",true, "test 12 failed" );
        }

        [TestMethod]
        public void TestMethod13()
        {
            User_account bmac = new User_account("Manager4", "1");
            BusinessManager bm = new BusinessManager(bmac, "Manager", "Manager", "054123456", "Manager@manage.com");
            dal.insertBusinessManager(bm);
            Business b = new Business("Rehovot", "5 star Spa", "Olympus", "SPA", bm);
            dal.insertBusiness(b);
            CouponInfo coupInf = new CouponInfo(b, 12345, 450, 300, new DateTime(2015, 06, 30), "package of 3 threatments for the amazing price of 100 each");
            String ans = dal.insertCouponInfo(coupInf);
            Assert.AreEqual(ans, "Coupon info added succsesfully", true, "test 11 failed");
        }
    }
}
