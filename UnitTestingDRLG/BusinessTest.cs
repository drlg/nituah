﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using DRLG;

namespace UnitTestingDRLG
{
    [TestClass]
    public class BusinessTest
    {


        mainData dal = mainData.getInstance();



      
        [TestMethod]
        public void TestMethod4()
        {
            User_account ua = new User_account("cafe1", "1");
            dal.insertUserAccount(ua);
            BusinessManager u = new BusinessManager(ua, "cafe", "bbb", "555", "rrr");
            dal.insertBusinessManager(u);
            Business b = new Business("beer-sheva", "cafe", "cafe", "food", u);

            string ans = dal.insertBusiness(b);
            Assert.AreEqual(ans, "Business added succesfully", true, "test4 failed");
        }


        [TestMethod]
        public void TestMethod5()
        {
            User_account ua = new User_account("cafe1", "1");
            dal.updateAddBusiness("Kiryat-Gat", "cafe1");
            Business ans = dal.findBusinessByUserName("cafe1");
            Assert.AreEqual(dal.updateAddBusiness("Kiryat-Gat", "cafe1"), "Address updated", true, "test5 failed");

        }

        [TestMethod]
        public void TestMethod6()
        {

            Business ans = dal.findBusinessByUserName("cafe1");
            Assert.AreEqual(ans.getBusMan().getUserName(), "cafe1", true, "test6 failed");
        }

        [TestMethod]
        public void TestMethod7()
        {

            Business ans = dal.findBusinessByName("cafe");
            Assert.AreEqual(ans.getName(), "cafe", true, "test7 failed");
        }
    }
}
