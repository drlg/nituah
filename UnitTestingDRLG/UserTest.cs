﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DRLG_GUI;
using System.Data.SqlClient;
using System.Windows;
using DAL.DRLG_couponDataSetTableAdapters;
using DAL;
using DRLG;




namespace UnitTestingDRLG
{


    [TestClass]
    public class UserTest
    {

   
        mainData dal = mainData.getInstance();



        [AssemblyCleanup()]
        public static void AssemblyCleanup()
        {
            mainData temp = mainData.getInstance();
            temp.deleteUser("Jennyl");
            temp.deleteUser("roie");
            temp.deleteBusiness("cafe1");
            temp.deleteOrderUser(1, "user1");
            temp.deleteOrderUser(1, "user2");
            temp.deleteOrderUser(1, "user3");
            temp.deleteUser("cafe1");
            temp.deleteAccount("user1");
            temp.deleteUser("user1");
            temp.deleteAccount("user2");
            temp.deleteUser("user2");
            temp.deleteAccount("user3");
            temp.deleteUser("user3");
            temp.deleteOrder(1);
            temp.removeCouponInfo(100);
            foreach (string s in temp.getCouponSerial(100))
            {
                temp.removeCoupon(s);
            }
            temp.deleteOrder(20);


        }




        [TestMethod]
        public void TestMethod1()
        {
            User_account ua=new User_account("Jennyl","1");
            User u = new User(ua, "Jenny", "Laskavi", "055555", "Email");
            dal.insertUser(u);
            User ans = dal.findUserByUserName("Jennyl");

            Assert.AreEqual(ans.getUserName(), "Jennyl", true, "test1 failed!!!");


        }

        [TestMethod]
        public void TestMethod2()
        {
            User_account ua = new User_account("roie", "1");
            User u = new User(ua, "Jenny", "Laskavi", "055555", "Email");
            string ans=dal.insertUser(u);

            

            Assert.AreEqual(ans, "added succesfully", true, "test2 failed!!!");

        }


        [TestMethod]
        public void TestMethod3()
        {
            Order o = new Order(1, "credit", new User("Admin", "", "", "", ""));
            dal.insertOrder(o);
            Order ans = dal.findOrderbyOrderId(1);

            Assert.AreEqual(ans.getID(), 1, 0, "test3 failed!!!");

        }
     
        [TestMethod]
        public void TestMethod8()
        {
            User_account ua = new User_account("user1", "1234");
            dal.insertUserAccount(ua);
            User u = new User(ua, "bbb", "bbb", "055555", "Email");
             dal.insertUser(u);
            String ans = dal.selectPasswordUser("user1");
            Assert.AreEqual(ans, "1234", true, "test8 failed");
        }
        [TestMethod]
        public void TestMethod9()
        {
            dal.updatePassUser("user1", "11");
            String ans = dal.selectPasswordUser("user1");
            Assert.AreEqual(ans, "11", true, "test9 failed!");
        }
        [TestMethod]
        public void TestMethod10()
        {
            dal.deleteAccount("user1");
            dal.deleteUser("user1");
            User ans = dal.findUserByUserName("user1");
            Assert.AreEqual(ans, null, "", "test10 failed!");
        }

    }
}
