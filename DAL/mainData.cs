﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRLG;
using System.Collections.ObjectModel;

namespace DAL
{
    public class mainData
    {

        private static mainData instance;
     

      
        static List<User> userList = new List<User>();
        static List<User_account> userAccList = new List<User_account>();
        static List<Order> orderList = new List<Order>();
        static List<Coupon> coupList = new List<Coupon>();
        static List<CouponInfo> coupInfoList = new List<CouponInfo>();
        static List<BusinessManager> BusinessUserList = new List<BusinessManager>();
        static List<Business> lBusiness = new List<Business>();
        static List<Admin> lAdmin = new List<Admin>();
       
        
        private mainData()
        {
            User_account adac=new User_account("Admin", "1");
            userAccList.Add(adac);
         Admin ad = new Admin(adac, "Admin", "Admin", "123456789", "Admin@Admin.com");
         lAdmin.Add(ad);
             User_account bmac=new User_account("Manager", "1");
            userAccList.Add(bmac);
         BusinessManager bm = new BusinessManager(bmac, "Manager", "Manager", "054123456", "Manager@manage.com");
         BusinessUserList.Add(bm);

         Business b = new Business("Rehovot", "5 star Spa", "Olympus", "Fashion", bm);
         lBusiness.Add(b);
         Business b1 = new Business("Beer-Sheva", "Reastaurant", "Aroma", "Food", bm);
         lBusiness.Add(b1);
         Business b2 = new Business("Rehovot", "Ski", "Ski", "Sport", bm);
         bm.addBus(b);
         bm.addBus(b1);
         bm.addBus(b2);
         lBusiness.Add(b2);
            User_account f1ac=new User_account("F1", "1");
            userAccList.Add(f1ac);
            User f1 = new User(f1ac, "F1", "F1", "0547894751", "F1@facebook.com");
            userList.Add(f1);
            User_account f2ac = new User_account("F2", "1");
            userAccList.Add(f2ac);
            User f2 = new User(f2ac, "F2", "F2", "0547894751", "F2@facebook.com");
            userList.Add(f2);
            f1.addFriend(f2);
            f2.addFriend(f1);
            f2.addFriend(bm);
            CouponInfo coupInf=new CouponInfo (b,123457,450,300,new DateTime(2015,06,30),"package of 3 threatments for the amazing price of 100 each");
            coupInfoList.Add(coupInf);
            coupInf.addCategory("Food");
            CouponInfo coupInf1 = new CouponInfo(b1, 123458, 250, 230, new DateTime(2015, 07, 01), "blablabla");
            coupInfoList.Add(coupInf1);
            coupInf1.addCategory("Sport");
            CouponInfo coupIn2f = new CouponInfo(b2, 123459, 750, 600, new DateTime(2015, 07, 30), "blablabla of 3 blablabla for the amazing price of 100 each");
            coupInfoList.Add(coupIn2f);
            coupIn2f.addCategory("Fashion");
            coupIn2f.addCategory("Food");
            Coupon coup = new Coupon("123456", 123457, coupInf);
            coupList.Add(coup);
            Order or = new Order(45, "Credit", f1);
            or.addCoupon(coup);
            or.addUser(f2);
            orderList.Add(or);

        }

        public static mainData getInstance()
        {
            if (instance == null)
            {
                instance = new mainData();
            }
            return instance;
        }
        
        public string insertUser(User usToadd)
        {
            User found=null;
            

                found = findUserByUserName(usToadd.getUserName());
                if (found == null)
                {
                    userList.Add(usToadd);
                    userAccList.Add(usToadd.getUserAccount());
                    return "added succesfully";
                }
                else
                {
                    return "user name is already in the system";
                }

        }

        public List<User> getUseresByCategory(String cat)
        {
            List<User> ans = new List<User>();
            foreach (User u in userList)
            {
                if (u.getPreferances().Contains(cat))
                {
                    ans.Add(u);
                }
            }
            return ans;
        }


        public string insertBusinessManager(BusinessManager bm)
        {
            BusinessManager found = null;


            found = findBusinessManByUserName(bm.getUserName());
            if (found == null)
            {
                BusinessUserList.Add(bm);
                return "added succesfully";
            }
            else
            {
                return "user name is already in the system";
            }
        }
        public User findUserByUserName(string UserName)
        {
            foreach (var v in userList)
            {
                if (v.getUserName().Equals(UserName))
                {
                    return v;
                }

            }
            return null;
        }
        public Admin findAdminByUserName(string userName)
        {
            foreach (var v in lAdmin)
            {
                if (v.getUserName().Equals(userName))
                {
                    return v;
                }

            }
            return null;

        }

        public ObservableCollection<String> findBusinessByOwner(String owner)
        {
            ObservableCollection<String> ans = new ObservableCollection<String>();
            BusinessManager bm = findBusinessManByUserName(owner);
            foreach (var v in bm.getBusiness())
            {
                ans.Add(v.getName());
            }
            return ans;
        }

        public ObservableCollection<String> getCouponInfoNames()
        {
            ObservableCollection<String> ans = new ObservableCollection<String>();

            foreach (var v in coupInfoList)
            {
                ans.Add(v.getID().ToString());
            }
            return ans;
        }

        public CouponInfo findCouponById(int id)
        {
            CouponInfo ans = null;
            foreach (var v in coupInfoList)
            {
                if (v.getID() == id)
                {
                    return v;
                }
            }
            return ans;
        }

        public List<Order> getOrdersByUser(String user)
        {
            List<Order> ans = new List<Order>();
            foreach(var b in orderList)
            {
                foreach (var v in b.getUsers())
                {
                    if (v.getUserName().Equals(user))
                    {
                        ans.Add(b);
                    }
                }
                
            }
            return ans;

        }
      /*  public string updatePermit(string userName,string per)
        {
            try
            {
                mUserAccountAdapter.UpdatePermitQuery(per, userName, userName);
                return "added succesfully";
            }
            catch (System.Data.SqlClient.SqlException e2)
            {
                return "Invalid permit";

            }
        }*/
        public string addPreference(string userName,string preference)
        {
            User found = null;


            found = findUserByUserName(userName);
            if (found != null)
            {
                found.addPreferance(preference);
                return "added succesfully";
            }
            else
            {
                return "user name doesn't exists";
            }
            
        }
        public CouponInfo findCouponInfoByCouponId(int couponid)
        {
            foreach (var v in coupInfoList)
            {
                if (v.getID()==couponid)
                {
                    return v;
                }

            }
            return null;
        }
        public Coupon findCouponBySerial(string Serial)
        {
            foreach (var v in coupList)
            {
                if (v.getSerial().Equals(Serial))
                {
                    return v;
                }

            }
            return null;
        }
        
        public string addCouponToCategory(int couponId, string category)
        {
            CouponInfo found = null;


            found = findCouponInfoByCouponId(couponId);
            if (found != null)
            {
                found.addCategory(category);
                return "added succesfully";
            }
            else
            {
                return "coupon info doesn't exists";
            }
        }
       
        public void addReqBus(Business b)
        {       
              
            Admin.addBusinessReq(b);
               
        }
        public void addReqCoup(CouponInfo coup)
        {

            Admin.addCouponReq(coup);
                
            
        }
      /*  public void insertBusinessCoupon(string user_name,int id)
        {

            mBusinessCouponAdapter.InsertQuery(user_name, id);
        }*/
        
        public string deleteUser(String userName)
        {
            User found = null;
            

            found = findUserByUserName(userName);
            if (found != null)
            {
                userList.Remove(found);
                deleteAccount(userName);
                return "user deleted succesfully";

            }
            else
            {
                return "user name doesn't exists";
            }
            
        }

        public string insertOrder(Order or)
        {
            Order found = null;


            found = findOrderbyOrderId(or.getID());
            if (found == null)
            {
                orderList.Add(or);
                return "order added succesfully";

            }
            else
            {
                return "order already exists";
            }
        }
        public Order findOrderbyOrderId(int orderid)
        {
            foreach (var v in orderList)
            {
                if (v.getID() == orderid)
                {
                    return v;
                }

            }
            return null;
        }
        /// <summary>
        /// //////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="couponId"></param>
        /// <returns></returns>
        public int getCouponPrice(int couponId)
        {
            CouponInfo found = null;
            found = findCouponInfoByCouponId(couponId);
            if (found != null)
            {
                return found.getPriceAfter();
            }
            else
            {
                return -1;
            }

        }
      

        public string deleteOrder(int id)
        {
            Order found = null;
            found = findOrderbyOrderId(id);
            if (found != null)
            {
                orderList.Remove(found);
                return "Order removed succesfully";
            }
            else{
                return"oredr doesn't exists";
            }
        }

        public Business findBusinessByUserName(string userName)
        {

            foreach (var v in lBusiness)
            {
                if (v.getBusMan().getUserName().Equals(userName))
                {
                    return v;
                }

            }
            return null;
        }


        public String updateAddBusiness(String newAddress, String userName)
        {
            Business found = null;
            found = findBusinessByUserName(userName);
            if(lBusiness.Count() == 1)
            {
                return lBusiness.ElementAt(0).ToString();
            }

            if (found != null)
            {
                found.setAdd(newAddress);
                return "Address updated";
            }
            else{
                return "Business doesn't exists";
            }
        }
      
      
       
        public Business findBusinessByName(string name)
        {
            foreach (var v in lBusiness)
            {
                if (v.getName().Equals(name))
                {
                    return v;
                }

            }
            return null;
        }


        public string insertBusiness(Business b)
        {
            Business found = null;
            found = findBusinessByUserName(b.getBusMan().getUserName());
            if (found == null)
            {
                lBusiness.Add(b);
                return "Business added succesfully";
            }
            else
            {
                return "Business already exists";
            }
        }

        public User_account findUserAccountByUserName(string userName)
        {
            foreach (var v in userAccList)
            {
                if (v.getUserName().Equals(userName))
                {
                    return v;
                }

            }
            return null;
        }
        public BusinessManager findBusinessManByUserName(string userName)
        {
            foreach (var v in BusinessUserList)
            {
                if (v.getUserName().Equals(userName))
                {
                    return v;
                }

            }
            return null;
        }

        public String selectPasswordUser(String userName)
        {
            User_account found = null;
            found = findUserAccountByUserName(userName);
            if (found != null)
            {

                return found.getPassword();
            }
            else
            {
                return "000000000";
            }

        }
       

        public string insertUserAccount(User_account us)
        {
            User_account found = null;
            found = findUserAccountByUserName(us.getUserName());
            if (found == null)
            {

                userAccList.Add(us);
                return "User Account added succsesfully";
            }
            else
            {
                return "UserName already exists";
            }
        }


        public string updatePassUser(String userName, String newPass)
        {
            User_account found = null;
            found = findUserAccountByUserName(userName);
            if (found != null)
            {

                found.setPassword(newPass);
                return "User Account password updated succsesfully";
            }
            else
            {
                return "UserName doesn't exists";
            }

        }

        public string deleteBusiness(String username)
        {
            Business found = null;
            found = findBusinessByUserName(username);
            if (found != null)
            {
                lBusiness.Remove(found);
                found.getBusMan().deleteBus(found);
                if (found.getBusMan().getBusiness().Count == 0)
                {
                    BusinessUserList.Remove(found.getBusMan());
                }

                return "Business deleted successfully";
            }
            else
            {
                return "Business doesn't exists";
            }
        }

        public string deleteAccount(String accountName)
        {
            User_account found = null;
            found = findUserAccountByUserName(accountName);
            if (found != null)
            {

                userAccList.Remove(found);
                return "User Acoount Removed successfully";
            }
            else
            {
                return "UserName doesn't exists";
            }
        }
        public string addUserToOrder(int orderId, string userName)
        {
            User found = null;
            found = findUserByUserName(userName);
            Order found2=null;
            found2=findOrderbyOrderId(orderId);
                        if (found2 != null && found != null)
                        {
                            found2.addUser(found);
                            return "User added successfully to order";
                        }
                        else
                        {
                            return "Something went wrong here";
                        }
        }

        public int countUsersOrder(int ID)
        {
            Order found = null;
            found = findOrderbyOrderId(ID);
            if (found != null)
            {
                return found.getUsers().Count;
            }
            else
            {
                return -1;
            }
           
        }

        public string deleteOrderUser(int ID, String user)
        {
            
            Order found2 = null;
            found2 = findOrderbyOrderId(ID);
            if (found2 != null )
            {
                foreach (var v in found2.getUsers())
                {
                    if (v.getUserName().Equals(user))
                    {
                        found2.getUsers().Remove(v);
                        return "User has been Ramoved successfully";
                    }
                }
                return "User doesn't Exists";
            }
            else
            {
                return "Order doesn't exists";
            }
        }
        public string removeCouponInfo(int id)
        {
            CouponInfo found = null;
            found = findCouponInfoByCouponId(id);
            if (found != null)
            {
                coupInfoList.Remove(found);
                return "coupon info removed successfully";
            }
            else return "no Coupon Info ";
        }
        public string removeCoupon(string Serial)
        {
            Coupon found = null;
            found = findCouponBySerial(Serial);
            if (found != null)
            {

                coupList.Remove(found);
                return "coupon removed successfully";
            }
            else return "no Coupon ";
        }
        public List<string> getCouponSerial(int id)
        {
            List<string> ans=new List<string>();
            foreach (var v in coupList)
            {
                if (v.getID() == id)
                {
                    ans.Add(v.getSerial());
                }
                
            }
            return ans;

        }
        public string insertCouponInfo(CouponInfo inf)
        {

            CouponInfo found = null;
            found = findCouponInfoByCouponId(inf.getID());
            if (found == null)
            {

                coupInfoList.Add(inf);
                return "Coupon info added succsesfully";
            }
            else
            {
                return "Coupon info already exists";
            }
        }
        public string insertCoupon(Coupon c)
        {
            Coupon found = null;
            found = findCouponBySerial(c.getSerial());
            if (found == null)
            {

                coupList.Add(c);
                return "Coupon added succsesfully";
            }
            else
            {
                return "Coupon already exists";
            }
        }
    public List<CouponInfo> getCouponInfo()
        {
            return coupInfoList;
        }
   public List<Order> getOrders()
    {
        return orderList;
    }
        public List<Business> getBusiness()
   {
       return lBusiness;
   }
        public void approveBus()
        {
            foreach (Business b in Admin.getBusinessRequests()) 
            {
                lBusiness.Add(b);
            }
            Admin.getBusinessRequests().Clear();
        }

        public List<CouponInfo> approveCoup()
        {
            List<CouponInfo> temp = new List<CouponInfo>();
            foreach (CouponInfo c in Admin.getCouponRequests())
            {
                coupInfoList.Add(c);
                temp.Add(c);
               
            }
            
            Admin.getCouponRequests().Clear();
            return temp;
        }
        public String getSerial2()
        {
            Random rand = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
            char[] buffer = new char[5];

            for (int i = 0; i < 5; i++)
            {
                buffer[i] = chars[rand.Next(chars.Length)];
            }
            return new string(buffer);
        }
        public void insertCouponToOrder(int orderid, int Couponid)
        {
            Order or = findOrderbyOrderId(orderid);
            CouponInfo ci=findCouponInfoByCouponId(Couponid);
            Coupon coup=new Coupon(getSerial2(),Couponid,ci);
            coupList.Add(coup);
            if (or != null)
            {
                or.addCoupon(coup);
            }
        }
        public List<CouponInfo> searchCouponsByDistance(string userName)
        {
            List<CouponInfo> ans = new List<CouponInfo>();
            User us = findUserByUserName(userName);
            if(us==null)
            {
                us = findAdminByUserName(userName);
            }
            
            if(us==null)
            {
                us = findBusinessManByUserName(userName);
            }
            if(us==null)
            {
                return null;
            }
            foreach (var v in coupInfoList)
            {
                foreach (var b in v.getBusiness())
                {
                    if (b.getLoc().distance_lest_than_ten(us.getLoc()))
                    {
                        ans.Add(v);
                    }
                }
            }
            return ans;
        }

        public ObservableCollection<String> searchCouponsByUserName(String uName)
        {
            ObservableCollection<String> ans = new ObservableCollection<String>();
            User us = findUserByUserName(uName);
            
            foreach(var v in getOrdersByUser(uName)){
                foreach (var b in v.getCoupons())
                {
                    if(b.getUsed() == false){
                    ans.Add(b.getSerial());
                        }
                }
            }
            return ans;
        }

        public ObservableCollection<String> getBusinessNames()
        {
            ObservableCollection<String> ans = new ObservableCollection<String>();
            foreach (var v in getBusiness())
            {
                
                        ans.Add(v.getName());
                    
                
            }
            return ans;
        }

          public List<CouponInfo> searchCouponsByLocation(string Location)
        {
           List<CouponInfo> ans = new List<CouponInfo>();
           
            foreach (var v in coupInfoList)
            {
                foreach (var b in v.getBusiness())
                {
                    if (b.getAdd().Equals(Location))
                    {
                        ans.Add(v);
                    }
                }
            }
            return ans;
        }
        public List<CouponInfo> searchCouponsByPref(string Pref)
        {
           List<CouponInfo> ans = new List<CouponInfo>();
           
            foreach (var v in coupInfoList)
            {
                foreach (var b in v.getCategory())
                {
                    if (b.Equals(Pref))
                    {
                        ans.Add(v);
                    }
                }
            }
            return ans;
        }
        public List<CouponInfo> searchCouponsByName(string bName)
        {
            List<CouponInfo> ans = new List<CouponInfo>();

            foreach (var v in coupInfoList)
            {
                foreach (var b in v.getBusiness())
                {
                    if (b.getName().Equals(bName))
                    {
                        ans.Add(v);
                    }
                }
            }
            return ans;
        }


     
    }
}
