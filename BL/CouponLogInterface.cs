﻿using DRLG;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    interface CouponLogInterface
    {

         int getPrice(int couponId);

         bool insertCoupon(CouponInfo ci);


         bool removeCouponInfo(int id);

         bool removeCoupon(int id);

         bool addCouponToCat(int coupId, string category);

         bool addCouponInfo(int idCoupon, string description, DateTime exp_date, int priceAfer, int origPrice, Business b);

         List<CouponInfo> searchCouponsByDistance(string userName);

         List<CouponInfo> searchCouponsByLocation(string Location);

         List<CouponInfo> searchCouponsByPref(string Pref);

         List<CouponInfo> searchCouponsByName(string bName);


         CouponInfo findCouponById(int id);


         ObservableCollection<String> searchCouponsByUserName(string uName);


         void updateRankBySerial(String serial, int rank);

         List<CouponInfo> getCouponInfo();


         ObservableCollection<String> getCouponInfoNames();


         void approveCoup();
      
    }
}
