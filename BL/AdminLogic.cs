﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRLG;

namespace BL
{
    public class AdminLogic
    {
        private mainData dataBase;
        public AdminLogic()
        {
            this.dataBase = mainData.getInstance();
        }
        public bool approveBusiness()
        {
            List<Business> list = DRLG.Admin.getBusinessRequests();
            foreach(Business b in list)
            {
               
                dataBase.insertBusiness(b);
                b.getBusMan().addBus(b);
            }
            return true;
        }
        public bool approveCoupon()
        {
            List<DRLG.CouponInfo> list = DRLG.Admin.getCouponRequests();
            foreach (DRLG.CouponInfo c in list)
            {
                dataBase.insertCoupon(new Coupon(dataBase.getSerial2(),c.getID(), c));
                dataBase.insertCouponInfo(c);
              
                
            }
            return true;
        }
            
            
    }
}
