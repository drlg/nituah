﻿using DRLG;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    interface BusinessLogInterface
    {
         bool addBusiness(String userName, String name, String address, String description, String category);

         bool addRequestBusiness(String userName, String name, String address, String description, String category);
       

         bool removeBusiness(string businessName);

         Business findBusinessByUserName(string userName);

         List<Business> getBusiness();

         void approveBus();


         Business findBusinessByName(String name);


         ObservableCollection<String> findBusinessByOwner(String owner);

         ObservableCollection<String> getBusinessNames();
     

    }
}
