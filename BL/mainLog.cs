﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using DRLG;

namespace BL
{

    public class mainLog : mainLogInterface
    {
        public  mainData dataBase= mainData.getInstance();
        public mainLog()
        {
           
        }
       
        public bool addUser(User_account ua,string FirstName,string LastName,string phoneNumber,string Email)
        {
            try
            {
                User u = new User(ua, FirstName, LastName, phoneNumber, Email);
                dataBase.insertUser(u);
                return true;
            }
            catch
            {
                return false;
            }
        }
      
        public bool addBusinessUser(string userName,string FirstName,string LastName,string phoneNumber,string Email)
        {
            try
            {
                BusinessManager u = new BusinessManager(dataBase.findUserAccountByUserName(userName), FirstName, LastName, phoneNumber, Email);
                dataBase.insertBusinessManager(u);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool addUserAccount(string userName,string pass)
        {
            try
            {
                dataBase.insertUserAccount(new User_account(userName,pass));
                return true;
            }
            catch
            {
                return false;
            }
        }
       public User findUserByUserName(string userName)
        {
            return dataBase.findUserByUserName(userName);
        }
       public User_account finUserAccountByUserName(string userName)
       {
           return dataBase.findUserAccountByUserName(userName);
       }
        public Admin findAdminByUserName(string userName)
       {
           return dataBase.findAdminByUserName(userName);
       }
       public BusinessManager findBusinessManByUserName(string userName)
        {
            return dataBase.findBusinessManByUserName(userName);
        }
        public bool selectPassUser(string userName, string pass)
        {
            try
            {
                if ((dataBase.selectPasswordUser(userName)).Equals(pass))
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;

            }
        }
      
       
        public bool addPreferenceToUser(string userName,string preference)
        {
            try
            {
                dataBase.addPreference(userName,preference);
                return true;
            }
            catch
            {
                return false;
            }
        }

      
        
    
        
      
        public bool removeUser(string userName)
        {
            try
           {
                dataBase.deleteUser(userName);
                return true;
            }
            catch
            {
                return false;
            }

        }
     
      
        public bool removeUserAccount(string userName)
        {
            try
            {
                dataBase.deleteAccount(userName);
                return true;
            }
            catch
            {
                return false;
            }
        }
  
      
        
    }
}
