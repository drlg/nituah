﻿using DRLG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    interface OrderLogInterface
    {
         bool insertToOrder(int ID, String date, String payment_method, User us);

         bool insertUserOrder(string userName, int orderId);

         bool removeOrderUser(int ID, string user);

         bool removeOrder(int ID);

         List<Order> getOrders();

         void insertCouponToOrder(int orderid, int Couponid);

         List<Order> getOrdersByUser(string logedUser);
      
    }
}
