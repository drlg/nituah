﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRLG;
using System.Collections.ObjectModel;

namespace BL
{
    public class CouponLogic : CouponLogInterface
    {
        private mainData dataBase;
        public CouponLogic()
        {
            this.dataBase = mainData.getInstance();
        }
        public int getPrice(int couponId)
        {
            try
            {

                return dataBase.getCouponPrice(couponId);
            }
            catch
            {
                return -1;
            }
        }
        public bool insertCoupon(CouponInfo ci)
        {
            try
            {
                Coupon c = new Coupon(dataBase.getSerial2(), ci.getID(), ci);
                dataBase.insertCoupon(c);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool removeCouponInfo(int id)
        {
            try
            {
                dataBase.removeCouponInfo(id);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool removeCoupon(int id)
        {
            try
            {
                foreach (string s in dataBase.getCouponSerial(id))
                {
                    dataBase.removeCoupon(s);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool addCouponToCat(int coupId, string category)
        {
            try
            {
                dataBase.addCouponToCategory(coupId, category);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool addCouponInfo(int idCoupon, string description, DateTime exp_date, int priceAfer, int origPrice, Business b)
        {
            try
            {
                CouponInfo c = new CouponInfo(b, idCoupon, origPrice, priceAfer, exp_date, description);
                dataBase.insertCouponInfo(c);
                return true;
            }
            catch
            {
                return false;
            }
           
        }
        public List<CouponInfo> searchCouponsByDistance(string userName)
        {
            return dataBase.searchCouponsByDistance(userName);
        }
        public List<CouponInfo> searchCouponsByLocation(string Location)
        {
            return dataBase.searchCouponsByLocation(Location);
        }
        public List<CouponInfo> searchCouponsByPref(string Pref)
        {
            return dataBase.searchCouponsByPref(Pref);
        }
        public List<CouponInfo> searchCouponsByName(string bName)
        {
            return dataBase.searchCouponsByName(bName);
        }

        public CouponInfo findCouponById(int id)
        {
            return dataBase.findCouponById(id);
        }

        public ObservableCollection<String> searchCouponsByUserName(string uName)
        {
            return dataBase.searchCouponsByUserName(uName);
        }

        public void updateRankBySerial(String serial, int rank)
        {
            Coupon cop = dataBase.findCouponBySerial(serial);
            cop.setRank(rank);
        }
        public List<CouponInfo> getCouponInfo()
        {
           return dataBase.getCouponInfo();
        }

        public ObservableCollection<String> getCouponInfoNames()
        {
            return dataBase.getCouponInfoNames();
        }

        public void approveCoup()
        {
            List<CouponInfo> temp = dataBase.approveCoup();
          
        }

        public List<User> AddCouponNotifications(int id, string category)
        {
            foreach (User u in dataBase.getUseresByCategory(category))
            {
                u.addNotification(dataBase.findCouponById(id));
            }
            return dataBase.getUseresByCategory(category);
        }

    }
}
