﻿using DRLG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    interface mainLogInterface
    {
         bool addUser(User_account ua, string FirstName, string LastName, string phoneNumber, string Email);


         bool addBusinessUser(string userName, string FirstName, string LastName, string phoneNumber, string Email);

         bool addUserAccount(string userName, string pass);

         User findUserByUserName(string userName);

         User_account finUserAccountByUserName(string userName);

         Admin findAdminByUserName(string userName);

         BusinessManager findBusinessManByUserName(string userName);

         bool selectPassUser(string userName, string pass);



         bool addPreferenceToUser(string userName, string preference);






         bool removeUser(string userName);



         bool removeUserAccount(string userName);
      
      
    }
}
