﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRLG;
using System.Collections.ObjectModel;

namespace BL
{
    public class BusinessLogic : BusinessLogInterface 
    {
        private mainData dataBase;

        public BusinessLogic()
        {
            this.dataBase = mainData.getInstance();
        }
        public bool addBusiness(String userName, String name, String address, String description, String category)
        {
            try
            {

                Business b = new Business(address, description, name, category, dataBase.findBusinessManByUserName(userName));
                dataBase.insertBusiness(b);
                return true;
            }
            catch
            {
                return false;
            }

        }
        public bool addRequestBusiness(String userName, String name, String address, String description, String category)
        {
            try
            {
                Business b = new Business(address, description, name, category, dataBase.findBusinessManByUserName(userName));
                dataBase.addReqBus(b);
                return true;

            }
            catch
            {
                return false;

            }

        }
    
        public bool removeBusiness(string businessName)
        {
            try
            {
                dataBase.deleteBusiness(businessName);
                return true;
            }
            catch
            {
                return false;
            }


        }
        public Business findBusinessByUserName(string userName)
        {
            return dataBase.findBusinessByUserName(userName);
        }
        public List<Business> getBusiness()
        {
            return dataBase.getBusiness();
        }
        public void approveBus()
        {
            dataBase.approveBus();
        }

        public Business findBusinessByName(String name)
        {
           return dataBase.findBusinessByName(name);
        }

        public ObservableCollection<String> findBusinessByOwner(String owner)
        {
            return dataBase.findBusinessByOwner(owner);
        }

        public ObservableCollection<String> getBusinessNames()
        {
            return dataBase.getBusinessNames();
        }

       
    }
}
