﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DRLG;

namespace BL
{
    public class OrderLogic : OrderLogInterface
    {
          private mainData dataBase;
          public OrderLogic()
        {
            this.dataBase = mainData.getInstance();
        }

        public bool insertToOrder(int ID, String date, String payment_method, User us)
        {
            try
            {
                Order o = new Order(ID, payment_method, us);
                dataBase.insertOrder(o);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool insertUserOrder(string userName, int orderId)
        {
            //try
            // {
            dataBase.addUserToOrder(orderId, userName);
            return true;
            /* }
             catch
             {
                 return false;
             }*/
        }
        public bool removeOrderUser(int ID, string user)
        {
            try
            {
                dataBase.deleteOrderUser(ID, user);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool removeOrder(int ID)
        {
            try
            {
                dataBase.deleteOrder(ID);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<Order> getOrders()
        {
            return dataBase.getOrders();
        }
        public void insertCouponToOrder(int orderid,int Couponid)
        {
            dataBase.insertCouponToOrder(orderid, Couponid);
        }
        public List<Order> getOrdersByUser(string logedUser)
        {
            return dataBase.getOrdersByUser(logedUser);
        }
    }
}
