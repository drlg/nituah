﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using BL;


namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for updatePrefWindow.xaml
    /// </summary>
    public partial class updatePrefWindow : Window
    {
        
        string logedUser = "";
        bool isUser;
        mainLog bl;
        public updatePrefWindow(string logedUser,mainLog bl,bool isUser)
        {
            InitializeComponent();
          
            this.logedUser = logedUser;
            this.isUser = isUser;
            this.bl = bl;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (isUser)
            {
                userLogedWindow us = new userLogedWindow(logedUser, bl, isUser, false);
                us.Show();
            }
            else
            {
                BusinessManagerLongedWindow b = new BusinessManagerLongedWindow(logedUser, bl);
                b.Show();
            }
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!(string.IsNullOrEmpty(notif_box.Text)))
            {
                if (notif_box.Text.Equals("On"))
                {
                    bl.findUserByUserName(logedUser).wantsNotifications = true;
                }
                else
                {
                    bl.findUserByUserName(logedUser).wantsNotifications = false;
                }
            }
        
            if (food.IsChecked.Value)
            {
                bl.addPreferenceToUser(logedUser, "Food");
            }
            if (sport.IsChecked.Value)
            {
                bl.addPreferenceToUser(logedUser, "Sport");
            }
            if (fashion.IsChecked.Value)
            {
                bl.addPreferenceToUser(logedUser, "Fashion");
            }
            if (isUser)
            {
                userLogedWindow us = new userLogedWindow(logedUser, bl, isUser, false);
                us.Show();
            }
            else
            {
                BusinessManagerLongedWindow b = new BusinessManagerLongedWindow(logedUser, bl);
                b.Show();
            }
            this.Close();
        }

    }
}
