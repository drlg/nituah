﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Facebook;
using System.Dynamic;


using System.Security.Policy;
using System.Reflection;
using System.Security;
using System.Security.Permissions;

namespace DRLG_GUI
{
    public partial class Form1 : Form
    {

        String AppId = Guid.NewGuid().ToString();
        //private const string AppId = "2424416217676";
        private Uri _loginUrl;
        private const string _ExtendedPermissions = "user_about_me,publish_stream,offline_access";
        FacebookClient fbClient = new FacebookClient();
        public Form1()
        {
            InitializeComponent();
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
            Login();
        }
        public void Login()
        {
            dynamic parameters = new ExpandoObject();
            parameters.client_id = AppId;
            parameters.redirect_uri = "https://www.facebook.com/connect/login_success.html";
            parameters.response_type = "token";
            parameters.display = "popup";

            if (!string.IsNullOrWhiteSpace(_ExtendedPermissions))
                parameters.scope = _ExtendedPermissions;

            var fb = new FacebookClient();
            _loginUrl = fb.GetLoginUrl(parameters);


            webBrowserLogin.Navigate(_loginUrl.AbsoluteUri);
        }

        private void webBrowserLogin_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
    }
}