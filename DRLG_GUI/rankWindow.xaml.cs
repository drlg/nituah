﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for rankWindow.xaml
    /// </summary>
    public partial class rankWindow : Window
    {

        String logedUser = "";
        mainLog bl;  
        CouponLogic coup = new CouponLogic();


        public rankWindow(String logedUser, mainLog bl)
        {
            this.logedUser = logedUser;
            this.bl = bl;
            this.ids = new ComboBox();
            InitializeComponent();
           
            foreach (var v in coup.searchCouponsByUserName(logedUser))
            {
                ComboBoxItem temp = new ComboBoxItem();
                temp.Content = v;
                ids.Items.Add(temp.Content.ToString());
                
            }
      
        }

        private void rank_Click(object sender, RoutedEventArgs e)
        {
            
            int rank = Int32.Parse(rank_box.Text);
            coup.updateRankBySerial(ids.SelectedItem.ToString(), rank);
            userLogedWindow us = new userLogedWindow(logedUser, bl, true, false);
            us.Show();
            this.Close();
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            userLogedWindow us = new userLogedWindow(logedUser, bl, true, false);
            us.Show();
            this.Close();
        }

       

       
    }
}
