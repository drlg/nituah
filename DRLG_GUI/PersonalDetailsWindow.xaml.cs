﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DRLG;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for PersonalDetailsWindow.xaml
    /// </summary>
    public partial class PersonalDetailsWindow : Window
    {
        string logedUser;
        mainLog bl;
        bool isBus;
        public PersonalDetailsWindow(mainLog bl,string logedUser,bool isBus)
        {
            InitializeComponent();
            this.bl = bl;
            this.logedUser = logedUser;
            this.isBus = isBus;
            List<User> l = new List<User>();
            l.Add(bl.findUserByUserName(logedUser));
            userDet.ItemsSource = l;
            List<String> list = bl.findUserByUserName(logedUser).getPreferances();
           
            foreach (var v in list)
            {
                
                if (v.Equals("Food"))
                {
                    
                    food_check.IsChecked = true;
                    food_check.IsEnabled = false;

                }
                if (v.Equals("Sport"))
                {
                    sport_check.IsChecked = true;
                    sport_check.IsEnabled = false;
                }
                if (v.Equals("Fashion"))
                {
                    fashion_check.IsChecked = true;
                    fashion_check.IsEnabled = false;
                }
            }
           
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(isBus)
            {
                BusinessManagerLongedWindow b = new BusinessManagerLongedWindow(logedUser, bl);
                b.Show();

            }
            else
            {
                userLogedWindow u = new userLogedWindow(logedUser, bl, true, false);
                u.Show();
            }
            this.Close();
        }
    }
}
