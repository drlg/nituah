﻿#pragma checksum "..\..\BusinessManagerLongedWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "590353185AEB69F081ADAA3BE5103FAA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DRLG_GUI {
    
    
    /// <summary>
    /// BusinessManagerLongedWindow
    /// </summary>
    public partial class BusinessManagerLongedWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\BusinessManagerLongedWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock displayUser;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\BusinessManagerLongedWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid couponData;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\BusinessManagerLongedWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addBus;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\BusinessManagerLongedWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addCoup;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\BusinessManagerLongedWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button remCoup;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\BusinessManagerLongedWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button logout;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\BusinessManagerLongedWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button edit;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\BusinessManagerLongedWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ranks;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DRLG_GUI;component/businessmanagerlongedwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\BusinessManagerLongedWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.displayUser = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.couponData = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 3:
            
            #line 14 "..\..\BusinessManagerLongedWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 15 "..\..\BusinessManagerLongedWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_2);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 16 "..\..\BusinessManagerLongedWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_3);
            
            #line default
            #line hidden
            return;
            case 6:
            this.addBus = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\BusinessManagerLongedWindow.xaml"
            this.addBus.Click += new System.Windows.RoutedEventHandler(this.addBus_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.addCoup = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\BusinessManagerLongedWindow.xaml"
            this.addCoup.Click += new System.Windows.RoutedEventHandler(this.addCoup_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.remCoup = ((System.Windows.Controls.Button)(target));
            return;
            case 9:
            this.logout = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\BusinessManagerLongedWindow.xaml"
            this.logout.Click += new System.Windows.RoutedEventHandler(this.logout_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.edit = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\BusinessManagerLongedWindow.xaml"
            this.edit.Click += new System.Windows.RoutedEventHandler(this.edit_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.ranks = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\BusinessManagerLongedWindow.xaml"
            this.ranks.Click += new System.Windows.RoutedEventHandler(this.ranks_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

