﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DRLG;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for addCouponWindow.xaml
    /// </summary>
    public partial class addCouponWindow : Window
    {
        
        String logedUser = "";
        mainLog bl;
        BusinessLogic busLog;
        CouponLogic cupLog;
        bool isAdmin;
        public addCouponWindow(String logedUser,mainLog bl)
        {

           
            this.bl = bl;
            this.logedUser = logedUser;
            this.busLog = new BusinessLogic();
            this.cupLog = new CouponLogic();
            this.business_box = new ComboBox();
             
            if(bl.findAdminByUserName(logedUser) != null)
            {
                isAdmin = true;
            }
            else
            {
                isAdmin = false;
            }
            InitializeComponent();

            if(!isAdmin){
            foreach (var v in busLog.findBusinessByOwner(logedUser))
            {
                ComboBoxItem temp = new ComboBoxItem();
                temp.Content = v;
                
                business_box.Items.Add(temp.Content.ToString());

            }
            }
            else
            {
                foreach (var v in busLog.getBusinessNames())
                {
                    ComboBoxItem temp = new ComboBoxItem();
                    temp.Content = v;

                    business_box.Items.Add(temp.Content.ToString());
                }
            }

        }
        private DateTime toDateTime(string dateTimeString)
        {
            dateTimeString = dateTimeString.Trim();

            DateTime dateTime = DateTime.MinValue;
            bool parsed = DateTime.TryParse(dateTimeString, out dateTime);
            //if dateTimeString parsed you then parsed == true else false.

            return dateTime;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                cupLog.addCouponInfo(1, text1.Text, cal.SelectedDate.Value, int.Parse(text4.Text), int.Parse(text3.Text), busLog.findBusinessByName(business_box.Text));
                string temp = cat.Text;

                cupLog.addCouponToCat(CouponInfo.getCID(), temp);
                cupLog.AddCouponNotifications(CouponInfo.getCID(), temp);



                if (!isAdmin)
                {
                    BusinessManagerLongedWindow ma = new BusinessManagerLongedWindow(logedUser, bl);
                    ma.Show();
                    this.Close();
                }
                else
                {
                    LogedWindow log = new LogedWindow(logedUser, bl);
                    log.Show();
                    this.Close();
                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("you must pick a date");
            }
           
        }

     
        private string getSerial()
        {
            Random rand = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
            char[] buffer = new char[10];

            for (int i = 0; i < 10; i++)
            {
                buffer[i] = chars[rand.Next(chars.Length)];
            }
            return new string(buffer);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if(!isAdmin){
            BusinessManagerLongedWindow us = new BusinessManagerLongedWindow(logedUser, bl);
            us.Show();
            this.Close();
            }
            else
            {
                LogedWindow log = new LogedWindow(logedUser, bl);
                log.Show();
                this.Close();
            }
        }

        
    }
}
