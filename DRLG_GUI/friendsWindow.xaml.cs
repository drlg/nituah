﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for friendsWindow.xaml
    /// </summary>
    public partial class friendsWindow : Window
    {
        bool isUser;
        mainLog bl;
        string logedUser;

        public friendsWindow(string logedUser,mainLog bl,bool isUser)
        {
            InitializeComponent();
            this.bl = bl;
            this.logedUser = logedUser;
            this.isUser = isUser;
            friends.ItemsSource = bl.findUserByUserName(logedUser).getFriends();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(isUser)
            {
                userLogedWindow u = new userLogedWindow(logedUser, bl, isUser, false);
                u.Show();


            }
            else
            {
                BusinessManagerLongedWindow bm = new BusinessManagerLongedWindow(logedUser, bl);
                bm.Show();
                

            }
            this.Close();
        }
    }
}
