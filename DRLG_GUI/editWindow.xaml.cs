﻿using BL;
using DRLG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for editWindow.xaml
    /// </summary>
    public partial class editWindow : Window
    {

        String logedUser = "";
        mainLog bl;
        BusinessLogic busLog;
        CouponLogic cupLog;
        bool isAdmin;
        
        public editWindow(String logedUser, mainLog bl, bool isAdmin)
        {
            InitializeComponent();
            this.bl = bl;
            this.logedUser = logedUser;
            this.busLog = new BusinessLogic();
            this.cupLog = new CouponLogic();
            this.isAdmin = isAdmin;

            if (!isAdmin)
            {
                targetbox.Text = "Business name";
                foreach (var v in busLog.findBusinessByOwner(logedUser))
                {
                    ComboBoxItem temp = new ComboBoxItem();
                    temp.Content = v;
                    listbox.Items.Add(temp.Content.ToString());

                }
            }
            else
            {
                foreach (var v in cupLog.getCouponInfoNames())
                {
                    ComboBoxItem temp = new ComboBoxItem();
                    temp.Content = v;
                    listbox.Items.Add(temp.Content.ToString());

                }
            }
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            bool prefs_changed = foodbox.IsChecked.Value | sportbox.IsChecked.Value | fashionbox.IsChecked.Value;
            bool desc_cjanged = !(descbox.Text.Equals(""));
                
            if (!isAdmin)
            {
                Business bs = busLog.findBusinessByName(listbox.Text);
                if(bs != null){
                if (prefs_changed)
                {
                    bs.setCategory(getChecked());
                }

                if (desc_cjanged)
                {
                   
                    bs.setDesc(descbox.Text);
                }
                }
                else
                {
                    MessageBox.Show("error, you must choose a business to edit");
                }
                BusinessManagerLongedWindow bm = new BusinessManagerLongedWindow(logedUser, bl);
                bm.Show();

            }
            else
            {
                CouponInfo cp = cupLog.findCouponById(Int32.Parse(listbox.Text));
                if (cp != null)
                {
                    if (prefs_changed)
                    {
                        cp.setCategory(getChecked());
                    }

                    if (desc_cjanged)
                    {
                        cp.setDesc(descbox.Text);
                    }
                }
                else
                {
                    MessageBox.Show("error, you must choose a coupon to edit");
                }
                LogedWindow log = new LogedWindow(logedUser, bl);
                log.Show();
            }

            this.Close();

        }

        private string getChecked()
        {
            if (foodbox.IsChecked.Value)
            {
                return "Food";
            }
            else if (sportbox.IsChecked.Value)
            {
                return "Sport";
            }
            else
            {
                return "Fashion";
            }
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
              if(!isAdmin)
            {
                BusinessManagerLongedWindow bm = new BusinessManagerLongedWindow(logedUser, bl);
                bm.Show();

            }
            else
            {
                LogedWindow log = new LogedWindow(logedUser,bl);
                log.Show();
            }
            
            this.Close();

        }

        private void food_box_Checked(object sender, RoutedEventArgs e)
        {
            sportbox.IsChecked = false;
            fashionbox.IsChecked = false;

        }

        private void sportbox_Checked(object sender, RoutedEventArgs e)
        {
            fashionbox.IsChecked = false;
            foodbox.IsChecked = false;
        }

        private void fashionbox_Checked(object sender, RoutedEventArgs e)
        {
            sportbox.IsChecked = false;
            foodbox.IsChecked = false;
        }



        
    }
}
