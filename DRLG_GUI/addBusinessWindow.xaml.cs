﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for addBusinessWindow.xaml
    /// </summary>
    public partial class addBusinessWindow : Window
    {
    
        String logedUser = "";
        mainLog bl;
        BusinessLogic businLog;
        bool isAdmin;
        public addBusinessWindow(String logedUser,mainLog bl,bool isAdmin)
        {
            InitializeComponent();
 
            this.bl = bl;
            this.logedUser = logedUser;
            this.businLog = new BusinessLogic();
            this.isAdmin = isAdmin;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            businLog.addRequestBusiness(logedUser, text1.Text, text2.Text, text4.Text, cat.Text);
            if (!isAdmin)
            {
                BusinessManagerLongedWindow b = new BusinessManagerLongedWindow(logedUser, bl);
                b.Show();
            }
          
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!isAdmin)
            {
                BusinessManagerLongedWindow b = new BusinessManagerLongedWindow(logedUser, bl);
                b.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Please add business");
            }
            
        }
    }
}
