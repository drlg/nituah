﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {
        
        

        String logedUser = "";
        mainLog bl = new mainLog();
        public MainWindow()
        {
            logedUser = "";
            InitializeComponent();
            
         
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
  
        }


   


        private void table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            SignUpWindow sign = new SignUpWindow(bl, false);
            sign.Show();
            this.Close();
            
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
      
            this.logedUser = text1.Text;

            if (bl.findUserByUserName(logedUser)==null && bl.findBusinessManByUserName(logedUser)==null && bl.findAdminByUserName(logedUser)==null)
            {
                ErrorUser.Visibility = System.Windows.Visibility.Visible;
                return;
            }
            if (!bl.selectPassUser(logedUser,text2.Text))
            {
                ErrorUser.Visibility = System.Windows.Visibility.Visible;
                return;
            }
           
            if (bl.findAdminByUserName(logedUser)!=null)
            {
                LogedWindow log = new LogedWindow(logedUser,bl);
                log.Show();
                MessageBox.Show("coupon's that expiered over a year ago are deleted now...");
                this.Close();
            }
            if (bl.findUserByUserName(logedUser)!=null)
            {
                userLogedWindow log = new userLogedWindow(logedUser, bl, true, true);
                log.displayUser.Text = logedUser;
                log.Show();
                this.Close();
            }
            if (bl.findBusinessManByUserName(logedUser)!=null)
            {
                BusinessManagerLongedWindow log = new BusinessManagerLongedWindow(logedUser, bl);
                log.displayUser.Text = logedUser;
                log.Show();
                this.Close();
            }
            
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Close();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            addBusinessWindow bu = new addBusinessWindow(logedUser, bl,false);
            bu.Show();
            this.Close();
        }

    }
}
