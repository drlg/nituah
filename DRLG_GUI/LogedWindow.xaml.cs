﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using BL;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for LogedWindow.xaml
    /// </summary>
    public partial class LogedWindow : Window
    {
        String userName = "";
        mainLog bl;
        BusinessLogic bus = new BusinessLogic();
        CouponLogic cup = new CouponLogic();
        public LogedWindow(String userName,mainLog bl)
        {
            InitializeComponent();
            this.userName = userName;
            this.bl = bl;
            
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow ma = new MainWindow();
            ma.Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            searchWindow sea = new searchWindow(userName, bl, "DRLG_Coupon_info",false);
            sea.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            cup.approveCoup();
            MessageBox.Show("coupons validated");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            bus.approveBus();
            MessageBox.Show("business's validated");
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            searchWindow sea = new searchWindow(userName, bl, "DRLG_Business",false);
            sea.Show();
            this.Close();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            rmvWindow rmvwin = new rmvWindow( bl, userName);
            rmvwin.Show();
            this.Close();
        }

        private void add_coupon_Click(object sender, RoutedEventArgs e)
        {
            addCouponWindow addC = new addCouponWindow(userName, bl);
            addC.Show();
            this.Close();
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            editWindow eWin = new editWindow(userName, bl, true);
            eWin.Show();
            this.Close();
        }

        private void add_bm_Click(object sender, RoutedEventArgs e)
        {
            SignUpWindow su = new SignUpWindow(bl, true);
            su.Show();
        }
    }
}
