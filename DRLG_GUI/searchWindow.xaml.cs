﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for searchWindow.xaml
    /// </summary>
    public partial class searchWindow : Window
    {
      
        String logedUser = "";
        mainLog bl;
        OrderLogic or = new OrderLogic();
        CouponLogic coup = new CouponLogic();
        BusinessLogic bus = new BusinessLogic();
        bool isUser;
        public searchWindow(String logedUser,mainLog bl,String tableToSearch,bool isUser)
        {
            InitializeComponent();
            this.logedUser = logedUser;
            this.bl = bl;
            if (tableToSearch.Equals("DRLG_Order") | tableToSearch.Equals("DRLG_Business"))
            {
                type_of_search.Visibility = System.Windows.Visibility.Hidden;
                showTableData(tableToSearch);
            }

            //showTableData(tableToSearch);
            this.isUser = isUser;
        }
        private void showTableData(String tableToSearch)
        {

            if (tableToSearch.Equals("DRLG_Order"))
            {
                datagrid1.ItemsSource = or.getOrdersByUser(logedUser);
            }
         //   if (tableToSearch.Equals("DRLG_Coupon_info"))
            //{
            //    datagrid1.ItemsSource = coup.getCouponInfo();
           // }
            if(tableToSearch.Equals("DRLG_Business"))
            {
                datagrid1.ItemsSource = bus.getBusiness();
            }
            

        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
            if(isUser)
            {
                userLogedWindow ma = new userLogedWindow(logedUser, bl, true, false);
                ma.Show();
            }
            else if(bl.findAdminByUserName(logedUser)==null)
            {
                BusinessManagerLongedWindow bm = new BusinessManagerLongedWindow(logedUser, bl);
                bm.Show();

            }
            else
            {
                LogedWindow log = new LogedWindow(logedUser,bl);
                log.Show();
            }
            
            this.Close();
        }

        

        private void type_of_search_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Distance.IsSelected)
            {
                text_block.Visibility = System.Windows.Visibility.Hidden;
                text_box.Visibility = System.Windows.Visibility.Hidden;
                prefs.Visibility = System.Windows.Visibility.Hidden;
                
            }
            if (City.IsSelected){
                text_block.Visibility = System.Windows.Visibility.Visible;
                text_box.Visibility = System.Windows.Visibility.Visible;
                prefs.Visibility = System.Windows.Visibility.Hidden;
            }

            if (Prefs.IsSelected){
                text_block.Visibility = System.Windows.Visibility.Hidden;
                text_box.Visibility = System.Windows.Visibility.Hidden;
                prefs.Visibility = System.Windows.Visibility.Visible;
            }

            if (Businessname.IsSelected)
            {
                text_block.Visibility = System.Windows.Visibility.Visible;
                text_box.Visibility = System.Windows.Visibility.Visible;
                prefs.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void search_button_Click(object sender, RoutedEventArgs e)
        {
            if (Distance.IsSelected)
            {
                datagrid1.ItemsSource = coup.searchCouponsByDistance(logedUser);
            }

            if (Businessname.IsSelected)
            {
                datagrid1.ItemsSource = coup.searchCouponsByName(text_box.Text);
            }

            if (Prefs.IsSelected)
            {
                if(Food.IsSelected){
                    datagrid1.ItemsSource = coup.searchCouponsByPref("Food");
                }
                if (Sport.IsSelected)
                {
                    datagrid1.ItemsSource = coup.searchCouponsByPref("Sport");
                }
                if (Fashion.IsSelected)
                {
                    datagrid1.ItemsSource = coup.searchCouponsByPref("Fashion");
                }
            }

            if (City.IsSelected)
            {
                datagrid1.ItemsSource = coup.searchCouponsByLocation(text_box.Text);
            }
        }
    }
}
