﻿using BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for BusinessManagerLongedWindow.xaml
    /// </summary>
    public partial class BusinessManagerLongedWindow : Window
    {
        
        string logedUser = "";
       
        mainLog bl;
        public BusinessManagerLongedWindow(string logedUser, mainLog bl)
        {
            InitializeComponent();
            displayUser.Text = logedUser;
            this.bl = bl;
            this.logedUser = logedUser;
          
            showCoupons();
        }
        private void showCoupons()
        {
          
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            updatePrefWindow up = new updatePrefWindow(logedUser, bl,false);
            up.Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow ma = new MainWindow();
            ma.Show();
            this.Close();
        }

        private void addCoup_Click(object sender, RoutedEventArgs e)
        {
            addCouponWindow addC = new addCouponWindow(logedUser, bl);
            addC.Show();
            this.Close();
        }

        private void addBus_Click(object sender, RoutedEventArgs e)
        {
            addBusinessWindow bu = new addBusinessWindow(logedUser, bl,false);
            bu.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            orderWindow or = new orderWindow(false, logedUser,  bl);
            or.Show();
            this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            searchWindow sea = new searchWindow(logedUser, bl, "DRLG_Order",false);
            sea.Show();
            this.Close();
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow ma = new MainWindow();
            ma.Show();
            this.Close();
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            editWindow eWin = new editWindow(logedUser, bl, false);
            eWin.Show();
            this.Close();
        }

        private void ranks_Click(object sender, RoutedEventArgs e)
        {
            rankOfBusinessWindow1 rwin = new rankOfBusinessWindow1(logedUser, bl);
            rwin.Show();
            this.Close();
        }
    }
}
