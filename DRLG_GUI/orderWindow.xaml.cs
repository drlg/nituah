﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DRLG;


namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for orderWindow.xaml
    /// </summary>
    public partial class orderWindow : Window
    {
        bool isUser;
        string logedUser;

        mainLog bl;
        OrderLogic or=new OrderLogic();
        CouponLogic coup;
        public orderWindow(bool isUser,string logedUser,mainLog bl)
        {
            InitializeComponent();
            this.bl = bl;
            this.isUser = isUser;
            this.logedUser = logedUser;
            this.coup = new CouponLogic();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (isUser)
            {
                userLogedWindow us = new userLogedWindow(logedUser, bl, isUser, false);
                us.Show();
            }
            else
            {
                BusinessManagerLongedWindow b = new BusinessManagerLongedWindow(logedUser, bl);
                b.Show();
            }
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            List<CouponInfo> cp = coup.getCouponInfo();
            bool legal_order = false;
            foreach(var v in cp){
                if (v.getID() == int.Parse(text1.Text))
                {
                    if (DateTime.Now.CompareTo(v.getExpDate()) < 0)
                    {
                        legal_order = true;
                    }
                }
            }
            if (legal_order)
            {
                or.insertToOrder(1, DateTime.Now.ToString(), text3.Text, bl.findUserByUserName(logedUser));
                // or.insertUserOrder(logedUser,int.Parse(text2.Text));
                or.insertCouponToOrder(Order.getCurrID() - 1, int.Parse(text1.Text));
            }

            else
            {
                MessageBox.Show("coupon is exipered, cannot buy it");
            }
            if (isUser) { 
            userLogedWindow us = new userLogedWindow(logedUser, bl, isUser, false);
            us.Show();
        }
            else
            {
                BusinessManagerLongedWindow b = new BusinessManagerLongedWindow(logedUser, bl);
                b.Show();
            }
            this.Close();

        }
    }
}
