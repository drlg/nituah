﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for rmvWindow.xaml
    /// </summary>
    public partial class rmvWindow : Window
    {
      
        mainLog bl;
        BusinessLogic busLog;
        CouponLogic cupLog;
        OrderLogic or;
        string logedUser;
        public rmvWindow(mainLog bl,string logedUser)
        {
            InitializeComponent();
            this.bl = bl;
            this.logedUser = logedUser;
            this.busLog = new BusinessLogic();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           
            if (User.IsSelected)
            {
                
                    bl.removeUserAccount(text1.Text);
                    bl.removeUser(text1.Text);

            }
            if (Business.IsSelected)
            {
                busLog.removeBusiness(text1.Text);
            }
            if (Coupon.IsSelected)
            {
                cupLog.removeCouponInfo(int.Parse(text2.Text));
                cupLog.removeCoupon(int.Parse(text2.Text));
            }
            if (Order.IsSelected)
            {
                or.removeOrder(int.Parse(text2.Text));
                or.removeOrderUser(int.Parse(text2.Text), text1.Text);
            }
              LogedWindow ma = new LogedWindow(logedUser,bl);
              ma.Show();
              this.Close();
        }

        private void table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            LogedWindow log = new LogedWindow(logedUser,bl);
            log.Show();
            this.Close();
        }

        
    }
}
