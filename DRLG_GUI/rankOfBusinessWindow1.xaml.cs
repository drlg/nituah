﻿using BL;
using DRLG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for rankOfBusinessWindow1.xaml
    /// </summary>
    public partial class rankOfBusinessWindow1 : Window
    {
        String logedUser = "";
        mainLog bl;
        BusinessLogic busLog;
        CouponLogic cupLog;

        class nested
        {
            public int id   { get; set; }
            public double rank   { get; set; }
            public int numOfRankers   { get; set; }

            public nested(int id, double rank, int numOfRankers)
            {
                this.id = id;
                this.rank = rank;
                this.numOfRankers = numOfRankers;
            }
        }
        public rankOfBusinessWindow1(String logedUser, mainLog bl)
        {
            InitializeComponent();
            this.bl = bl;
            this.logedUser = logedUser;
            this.busLog = new BusinessLogic();
            this.cupLog = new CouponLogic();
            List<nested> nestedList = new List<nested>();
            datagrid.ItemsSource = nestedList;
            
            foreach (var v in busLog.findBusinessByOwner(logedUser))
            {
               
                ComboBoxItem temp = new ComboBoxItem();
                temp.Content = v;
                
                listbox.Items.Add(temp.Content.ToString());

            }
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            BusinessManagerLongedWindow ma = new BusinessManagerLongedWindow(logedUser, bl);
            ma.Show();
            this.Close();
        }

      

        private void listbox_DropDownClosed(object sender, EventArgs e)
        {
            List<nested> nestedList = new List<nested>();

            
            List<CouponInfo> infoList = cupLog.searchCouponsByName(listbox.Text);
            foreach (var v in infoList)
            {
                nested temp = new nested(v.getID(), v.getTotalRank(), v.getNumOfRankers());
                nestedList.Add(temp);
            }
            datagrid.ItemsSource = nestedList;
            datagrid.Items.Refresh();
        }

        private void listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
