﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for addUserWindow.xaml
    /// </summary>
    public partial class addUserWindow : Window
    {
        SqlConnection con;
        public addUserWindow(SqlConnection con)
        {
            InitializeComponent();
            this.con = con;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlCommand com = new SqlCommand("INSERT into DRLG_User (UserName,FirstName,LastName,phoneNumber,email) values ('" + text1.Text + "','" + text2.Text + "','" + text3.Text + "','"+text4.Text+"','"+text5.Text+"')", con);
            com.ExecuteNonQuery();
            MainWindow ma = new MainWindow();
            ma.Show();
            this.Close();
        }
    }
}
