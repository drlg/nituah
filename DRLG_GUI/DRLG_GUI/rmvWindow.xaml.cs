﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for rmvWindow.xaml
    /// </summary>
    public partial class rmvWindow : Window
    {
        SqlConnection con;
        public rmvWindow(SqlConnection con)
        {
            InitializeComponent();
            this.con = con;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlCommand com=null;
            if (User.IsSelected)
            {
                com = new SqlCommand("DELETE  FROM DRLG_User WHERE UserName='"+text1.Text+"'", con);
            }
            if (UserAccount.IsSelected)
            {
                com = new SqlCommand("DELETE  FROM DRLG_UserAccount WHERE UserName='" + text1.Text + "'", con);
            }
            if (Business.IsSelected)
            {
                com = new SqlCommand("DELETE  FROM DRLG_Business WHERE name='" + text1.Text + "'", con);
            }
            if (Coupon.IsSelected)
            {
                com = new SqlCommand("DELETE  FROM DRLG_Coupon WHERE name='" + text1.Text + "'", con);
            }
              
              com.ExecuteNonQuery();
              MainWindow ma = new MainWindow();
              ma.Show();
              this.Close();
        }

        private void table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
