﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for addCouponWindow.xaml
    /// </summary>
    public partial class addCouponWindow : Window
    {
        SqlConnection con;
        public addCouponWindow(SqlConnection con)
        {
            InitializeComponent();
            this.con = con;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlCommand com = new SqlCommand("INSERT into DRLG_Coupon (name,address,city,description,category) values ('" + text1.Text + "','" + text2.Text + "','" + text3.Text + "','" + text4.Text + "','" + text5.Text +"','"+text6.Text+"','"+text7.Text+"')", con);
            com.ExecuteNonQuery();
            MainWindow ma = new MainWindow();
            ma.Show();
            this.Close();
        }
    }
}
