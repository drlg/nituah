﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for searchWindow.xaml
    /// </summary>
    public partial class searchWindow : Window
    {
        SqlConnection con;
        public searchWindow(SqlConnection con)
        {
            InitializeComponent();
            this.con = con;
        }

   

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
           
            SqlCommand com=null;
            if(User.IsSelected)
            {
                com = new SqlCommand("SELECT * FROM DRLG_User WHERE UserName='" + text1.Text + "'", con);

            }
            if(UserAccount.IsSelected)
            {
                com = new SqlCommand("SELECT* FROM DRLG_UserAccount WHERE UserName='" + text1.Text + "'", con);
               
            }
            if(com!=null)
            com.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable table = new DataTable();
            da.Fill(table);
            datagrid1.ItemsSource = table.DefaultView;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow ma = new MainWindow();
            ma.Show();
            this.Close();
        }

        private void table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
