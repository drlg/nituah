﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {
        
        
        SqlConnection con = new SqlConnection
           (@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\DRLG_coupon.mdf;Integrated Security=True");
        

        public MainWindow()
        {
            
            InitializeComponent();
            con.Open();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            

            if (user.IsSelected)
            {
                addUserWindow addwin = new addUserWindow(con);
                addwin.Show();
                this.Close();

            }
            if (userAccount.IsSelected)
            { 
                addAccWindow addwin = new addAccWindow(con);
                addwin.Show();
                this.Close();

            }
          
            if (business.IsSelected)
            {
                addBusinessWindow addwin = new addBusinessWindow(con);
                addwin.Show();
                this.Close();

            }
             if (coupon.IsSelected)
            {
                addCouponWindow addwin = new addCouponWindow(con);
                addwin.Show();
                this.Close();

            }
        }


        private void Search_Click(object sender, RoutedEventArgs e)
        {


            searchWindow searchwin = new searchWindow(con);
            searchwin.Show();
            this.Close();

           
         
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            rmvWindow rmvwin = new rmvWindow(con);
            rmvwin.Show();
            this.Close();
        }

        private void table_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }
    }
}
