﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using BL;
using DRLG;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for userLogedWindow.xaml
    /// </summary>
    public partial class userLogedWindow : Window
    {
   
        string logedUser = "";
        bool isUser;
        mainLog bl;
        CouponLogic cup=new CouponLogic();

        class nested
        {
          

            public int originalPrice { get; set; }
            public int priceAfterDiscount { get; set; }
            public int num_of_rankers { get; set; }
            public double totalRank { get; set; }
            public DateTime expDate { get; set; }
            public String description { get; set; }
            public int id { get; set; }
            public String category { get; set; }
            public string business { get; set; }

            public nested(int id, int originalPrice, int priceAfterDiscount, int num_of_rankers, double totalRank, DateTime expDate, string description, string category, string business)
            {
                this.id = id;
                this.originalPrice = originalPrice;
                this.priceAfterDiscount = priceAfterDiscount;
                this.num_of_rankers = num_of_rankers;
                this.totalRank = totalRank;
                this.expDate = expDate;
                this.description = description;
                this.category = category;
                this.business = business;
                
            }
   
          
        }


        public userLogedWindow(string logedUser,mainLog bl,bool isUser, bool needToCheckNotifications)
        {
            InitializeComponent();
            couponData.Visibility = System.Windows.Visibility.Hidden;
            notifications_box.Visibility = System.Windows.Visibility.Hidden;
            this.bl = bl;
            this.logedUser = logedUser;
            this.isUser = isUser;
            displayUser.Text = logedUser;
            List<nested> nestedList = new List<nested>();


            if (needToCheckNotifications & (bl.findUserByUserName(logedUser).getNotifications().Count != 0) & (bl.findUserByUserName(logedUser).wantsNotifications == true))
            {
                
                DateTime now = DateTime.Now;
                TimeSpan span = (now - bl.findUserByUserName(logedUser).getLastLogin()).Duration();
                bl.findUserByUserName(logedUser).setLastLogin();
                
                if(span.TotalMilliseconds > 10){
                    showCoupons(nestedList);
                }
                
            }
            
        }

        private void showCoupons(List<nested>  nestedList)
        {
            List<CouponInfo> temp = bl.findUserByUserName(logedUser).getNotifications();
            foreach (CouponInfo v in temp)
            {
                nested temp1 = new nested(v.getID(), v.getOrigiPrice(), v.getPriceAfter(),v.getNumOfRankers(), v.getTotalRank(), v.getExpDate(), v.getDesc(), v.getCategory().ElementAt(0),v.getBusiness().ElementAt(0).getName() );
                nestedList.Add(temp1);
            }
            couponData.ItemsSource = nestedList;
            couponData.Items.Refresh();
            couponData.Visibility = System.Windows.Visibility.Visible;
            notifications_box.Visibility = System.Windows.Visibility.Visible;
            
            bl.findUserByUserName(logedUser).emptyNotification();
        }
       

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            updatePrefWindow up = new updatePrefWindow(logedUser,bl, isUser);
            up.Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow ma = new MainWindow();
            ma.Show();
            this.Close();
        }

        private void addCoup_Click(object sender, RoutedEventArgs e)
        {
            addCouponWindow addC = new addCouponWindow(logedUser, bl);
            addC.Show();
            this.Close();
        }

        private void addBus_Click(object sender, RoutedEventArgs e)
        {
            addBusinessWindow bu = new addBusinessWindow(logedUser, bl,false);
            bu.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            orderWindow or = new orderWindow(isUser, logedUser,  bl);
            or.Show();
            this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            searchWindow sea = new searchWindow(logedUser,  bl, "DRLG_Order",true);
            sea.Show();
            this.Close();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            PersonalDetailsWindow p = new PersonalDetailsWindow(bl, logedUser, false);
            p.Show();
            this.Close();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            friendsWindow friends = new friendsWindow(logedUser, bl, true);
            friends.Show();
            this.Close();
        }

        private void search_button_Click(object sender, RoutedEventArgs e)
        {
            searchWindow sea = new searchWindow(logedUser, bl, "DRLG_Coupon_info", true);
            sea.Show();
            this.Close();
        }

        private void rank_Click(object sender, RoutedEventArgs e)
        {
            rankWindow rw = new rankWindow(logedUser, bl);
            rw.Show();
            this.Close();
        }
    }
}
