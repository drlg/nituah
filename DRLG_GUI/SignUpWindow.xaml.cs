﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using BL;

namespace DRLG_GUI
{
    /// <summary>
    /// Interaction logic for SignUpWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
     
        private mainLog bl;
        bool isAdmin;
        public SignUpWindow(mainLog bl, bool isAdmin)
        {
            InitializeComponent();
            this.isAdmin = isAdmin;
            this.bl = bl;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            if (food.IsChecked.Value | sport.IsChecked.Value | fashion.IsChecked.Value)
            {
           
            bl.addUserAccount(text1.Text,text2.Text);
           
            if (!isAdmin)
            {
                bl.addUser(bl.finUserAccountByUserName(text1.Text), text3.Text, text4.Text, text5.Text, text6.Text);
            }
            else
            {
                bl.addBusinessUser(text1.Text, text3.Text, text4.Text, text5.Text, text6.Text);
            }

            if (food.IsChecked.Value)
            {
                
                bl.addPreferenceToUser(text1.Text, "Food");
            }
            if (sport.IsChecked.Value)
            {
                
                bl.addPreferenceToUser(text1.Text, "Sport");
            }
            if (fashion.IsChecked.Value)
            {
                
                bl.addPreferenceToUser(text1.Text, "Fashion");
            }
          }
            else
            {
                MessageBox.Show("error, must choose at least one preference");
            }
            if (!isAdmin)
            {
                MainWindow ma = new MainWindow();
                ma.Show();
            }
            else 
            {
                addBusinessWindow ab = new addBusinessWindow(text1.Text, bl, true);
                ab.Show();
            }
    
            this.Close();
        }

        private void table_PermitChange(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
