﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class Location
    {
        double altitude;
        double longitude;

        public Location()
        {
            this.altitude = 0;
            this.longitude = 0;
        }
        public Location(double alt,double longi)
    {
        this.altitude = alt;
            this.longitude=longi;
    }   
        public double getAltitue()
        {
            return this.altitude;
        }
        public double getLongitude()
        {
            return this.longitude;
        }
        public void setAlt(double newA)
        {
            this.altitude = newA;
        }
        public void setLong(double newL)
        {
            this.longitude = newL;
        }

        public bool distance_lest_than_ten(Location other)
        {
            return (Math.Sqrt(Math.Pow(getAltitue() - other.getAltitue(),2) + Math.Pow(getLongitude() - other.getLongitude(), 2)) < 6);
        }
        
    }
}
