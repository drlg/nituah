﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class User
    {
        public String firstName { get; set; }
        public String LastName { get; set; }
        public String PhoneNumber { get; set; }
        public String email { get; set; }
        public Location loc { get; set; }
        public User_account userAcc { get; set; }
        public List<User> friends { get; set; }
        public List<String> preferances { get; set; }

        public List<CouponInfo> notifications { get; set; }

        public DateTime lastLogin { get; set; }
        public Boolean wantsNotifications { get; set; }

        




        public User(User_account usAcc, String firstName, String lastName, String phoneNumber, String email)
        {
            this.firstName = firstName;
            this.LastName = lastName;
            setPhoneNumber(phoneNumber);
            setEmail(email);
            Random r = new Random();
            loc = new Location(r.Next(20),r.Next(20));
            this.userAcc = usAcc;
            friends = new List<User>();
            preferances = new List<String>();
            notifications = new List<CouponInfo>();
            lastLogin = DateTime.Now;
            wantsNotifications = true;

        }
        public User(String userName, String firstName, String lastName, String phoneNumber, String email,String password)
        {
            this.firstName = firstName;
            this.LastName = lastName;
            setPhoneNumber(phoneNumber);
            setEmail(email);
            Random r = new Random();
            loc = new Location(r.Next(20), r.Next(20));
            this.userAcc = new User_account(userName, password);
            friends = new List<User>();
            preferances = new List<String>();
            notifications = new List<CouponInfo>();
            lastLogin = DateTime.Now;
            wantsNotifications = true;
        }
        public User(String userName, String firstName, String lastName, String phoneNumber, String email)
        {
            this.firstName = firstName;
            this.LastName = lastName;
            setPhoneNumber(phoneNumber);
            setEmail(email);
            loc = null;
            this.userAcc = null;
            wantsNotifications = true;
        }
        //getters

        public String getFirstName()
        {
            return this.firstName;
        }
        public String getLastName()
        {
            return this.LastName;
        }

        public Location getLoc()
        {
            return this.loc;
        }
        public void setLoc(Location loc)
        {
            this.loc = loc;

        }
        public String getPhoneNumber()
        {
            return this.PhoneNumber;
        }
        public String getEmail()
        {
            return this.email;
        }
        public String getUserName()
        {
            return this.userAcc.getUserName();
        }
        public User_account getUserAccount()
        {
            return this.userAcc;
        }
        public List<User> getFriends()
        {
            return this.friends;
        }
        public List<String> getPreferances()
        {
            return this.preferances;
        }
        //setters

        public void setFirstName(String firstName)
        {
            this.firstName = firstName;
        }
        public void setLastName(String lastName)
        {
            this.LastName = lastName;
        }

        public void addFriend(User friend)
        {
            this.friends.Add(friend);
        }
        public void addPreferance(String Pref)
        {
            this.preferances.Add(Pref);
        }
        public void setPhoneNumber(String phoneNumber)
        {
            this.PhoneNumber = phoneNumber;
           /* int DigitsOfNumber = numDigits(phoneNumber);
            if (DigitsOfNumber == 9 || DigitsOfNumber == 10)
                this.PhoneNumber = phoneNumber;
            else
                throw new System.ArgumentException("Please enter a correct PhoneNumber");*/
        }

        public void setLastLogin()
        {
            lastLogin = DateTime.Now;
        }

        public DateTime getLastLogin()
        {
            return lastLogin;
        }

        public List<CouponInfo> getNotifications()
        {
            return notifications;
        }

        

        public void emptyNotification()
        {
            notifications.Clear();
        }
        public void setEmail(String email)
        {
            this.email = email;
         /*   if (email.Contains("@") && email.Contains("."))
                this.email = email;
            else
                throw new System.ArgumentException("Please enter a correct Email");*/
        }
        private static int numDigits(String s)
        {
            int count = 0;
            int x = s.Length;
            while (x > 0)
            {
                x = x / 10;
                count = count + 1;
            }
            return count;
        }




        public override string ToString()
        {
            if (this == null)
            {
                return " ";
            }
            else
            {
                return "The user's name is: " + this.getFirstName() + " " + this.getLastName() + "\n" + " email: " + this.email + "\n" + " Phone number: " + "0" + this.PhoneNumber;
            }

        }
    
public void addNotification(CouponInfo couponInfo)
{
    notifications.Add(couponInfo);
}}
}
