﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class Order
    {
        private static int _ID = 1; 
        public double totalPrice { get; set; }
        public DateTime date { get; set; }
        public String paymentMethod { get; set; }
        public int id { get; set; }
        public List<Coupon> coupons { get; set; }
        public List<User> users { get; set; }

         public Order(int ID, String pay,User us)
        {
            this.totalPrice = 0;
            this.date = DateTime.Today;
            this.id=_ID;
            this.paymentMethod = pay;
            this.coupons = new List<Coupon>();
            this.users = new List<User>();
            users.Add(us);
            _ID++;
        }

        public static int getCurrID()
        {
            return _ID;
        }

         public void addCoupon(Coupon coup)
         {
             this.coupons.Add(coup);
             this.totalPrice = totalPrice + coup.getInfo().getPriceAfter();
         }
         public void addUser(User us)
         {
             this.users.Add(us);
             if (this.users.Count == 5)
             {
                 this.totalPrice = totalPrice * 0.9;
             }
         }

         public DateTime getDate()
         {
             return date;
         }
         public String getPayMethod()
         {
             return this.paymentMethod;
         }
         public int getID()
         {
             return this.id;
         }
         public double getTotalPrice()
         {
             return this.totalPrice;
         }

         public List<Coupon> getCoupons()
         {
             return this.coupons;
         }
         public List<User> getUsers()
         {
             return this.users;
         }
         public void setPayMethod(String pay)
         {
             this.paymentMethod = pay;
         }
         public override string ToString()
         {
             if (this == null)
             {
                 return " ";
             }
             else
             {
                 return " This Order's ID is " + this.getID() + "\n" + " his date is: " + this.getDate().ToString() + "\n" + " totalPrice: " + this.totalPrice + "\n" + " paymentMethod: "  + this.paymentMethod;
             }

         }
    }
}
