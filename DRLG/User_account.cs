﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class User_account
    {
      
        public String userName { get; set; }
        public String password { get; set; }
        public Boolean permit { get; set; }

        

        public User_account(String userName, String password)
        {
            
            this.userName = userName;
            this.password = password;
            this.permit = false;
            

        }


        public String getUserName()
        {
            return this.userName;
        }
        public String getPassword()
        {
            return this.password;
        }

        public void setPassword(String pass)
        {
            while (pass.Equals(""))
                Console.WriteLine("No empty combinations will be accepted as a password");
            this.password = pass;
        }

        public void setPermit(Boolean newPer)
        {
            this.permit = newPer;
        }

        public Boolean getPermit()
        {
            return this.permit;
        }

    }
}
