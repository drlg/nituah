﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class CouponInfo
    {
        private static int _ID = 1;
        public int originalPrice { get; set; }
        public int priceAfterDiscount { get; set; }
        public int num_of_rankers { get; set; }
        public double totalRank { get; set; }
        public DateTime expDate { get; set; }
        public String description { get; set; }
        public int id { get; set; }
        public List<String> category { get; set; }
        public List<Business> business { get; set; }

        public CouponInfo(Business us, int ID, int originalPrice, int PriceAfter, DateTime expDate, String desc)
        {
            this.originalPrice = originalPrice;
            this.priceAfterDiscount = PriceAfter;
            this.id=_ID;
            this.expDate = expDate;
            this.description = desc;
            this.num_of_rankers = 0;
            this.totalRank = 0;
            this.category = new List<String>();
            this.business = new List<Business>();
            business.Add(us);
            _ID++;
        }

        public int getOrigiPrice()
        {
            return this.originalPrice;
        }
        public int getPriceAfter()
        {
            return this.priceAfterDiscount;
        }
        public double getTotalRank()
        {
            return this.totalRank;
        }
        public int getNumOfRankers()
        {
            return this.num_of_rankers;
        }
        public String getDesc()
        {
            return this.description;
        }

        public int getID()
        {
            return this.id;
        }

        public static int getCID() {
            return _ID - 1;
        }

        public DateTime getExpDate()
        {
            return this.expDate;
        }

        public List<String> getCategory()
        {
            return this.category;
        }

        public List<Business> getBusiness()
        {
            return this.business;
        }

        public void addBusiness(Business us)
        {
            this.business.Add(us);

        }
        public void addCategory(String cat)
        {
            
            this.category.Add(cat);
        }

        public void setCategory(String cat)
        {
            clearCat();
            category.Add(cat);
        }

        public void clearCat()
        {
            category.Clear();
        }

        public void setDesc(String desc)
        {
            description = desc;
        }
        

        public void updateRank(int rank)
    {
        if (this.num_of_rankers == 0)
        {
            this.num_of_rankers=1;
            this.totalRank = rank;

        }
        else
        {
            double temp=this.totalRank*this.num_of_rankers;
            this.num_of_rankers = this.num_of_rankers + 1;
            temp = temp + rank;
            this.totalRank = temp / this.num_of_rankers;
        }
    }
        public override string ToString()
        {
            if (this == null)
            {
                return " ";
            }
            else
            {
                return " This Coupon's ID is " + this.getID() + "\n" + " his original Price was: " + this.getOrigiPrice() + "\n" + " his proce after discount is: " + this.getPriceAfter() + "\n" + " rank: " + this.getTotalRank();
            }

        }
    }
}
