﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class Coupon
    {

        public String Serial { get; set; }
        public Boolean Used { get; set; }
        public int ID { get; set; }
        public int rank { get; set; }
        public CouponInfo info { get; set; }
     

           public Coupon(String Serial,int ID,CouponInfo info)
        {
            this.Serial = Serial;
            this.ID = ID;
            this.info = info;
            this.rank = 0;
            this.Used = false;
         

        }

           public String getSerial()
           {
               return Serial;
           }

           public int getID()
           {
               return ID;
           }
           public int getRank()
           {
               return rank;
           }
           public Boolean getUsed()
           {
               return Used;
           }
           public CouponInfo getInfo()
           {
               return info;
           }
      

        public void setRank(int rank)
           {
               if (rank >= 0 && rank < 6)
               {
                   this.rank = rank;
                   this.Used = true;
                   this.info.updateRank(rank);
               }
           }
        public override string ToString()
        {
            if (this == null)
            {
                return " ";
            }
            else
            {
                return " This Coupon's ID is " + this.getID() + "\n" + " his Serial is: " + this.getSerial() + "\n" + " rank: " + this.getRank();
            }

        }

    }
}
