﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class Business
    {
        public String business_address { get; set; }
        public String business_description { get; set; }
        public String name {get; set;}
        public String category { get; set; }
        public BusinessManager bm { get; set; }
        private Location loc { get; set; }

        public Business(String address, String description, String name, String category,BusinessManager bm)
        {
            this.name = name;
            this.category = category;
            this.business_description = description;
            this.business_address = address;
            this.bm = bm;
            Random r = new Random();
            loc = new Location(r.Next(20), r.Next(20));
        }

        public void setAdd(String add)
        {
            this.business_address = add;
        }
        public String getAdd()
        {
            return this.business_address;
        }
        public Location getLoc()
        {
            return this.loc;
        }
        public String getDesc()
        {
            return this.business_description;
        }

        public void setDesc(String newDesc)
        {
            business_description = newDesc;
        }
        public String getName()
        {
            return this.name;
        }
        public String getCategory()
        {
            return this.category;
        }

        public void setCategory(String newCat)
        {
            category = newCat;
        }
        public BusinessManager getBusMan()
        {
            return this.bm;
        }

        public String ToString()
        {
            return "name: " + getName();
        }
    }
}
