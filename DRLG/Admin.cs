﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class Admin : User
    {
        public static List<Business> BusinessToValid = new List<Business>(); 
        public static List<CouponInfo> CouponsToValid = new List<CouponInfo>();
        public String firstName { get; set; }
        public String LastName { get; set; }
        public String PhoneNumber { get; set; }
        public String email { get; set; }



        public Admin(User_account usAcc,String firstName, String lastName, String phoneNumber, String email)
            : base(usAcc,firstName, lastName, phoneNumber, email)
        {

            
        }
        //getters
        public static List<Business> getBusinessRequests()
        {
            return BusinessToValid;
        }
      
        public static List<CouponInfo> getCouponRequests()
        {
            return CouponsToValid;
        }
        
        //setters
        public static void setAdminRequests(List<Business> setReq)
        {
            BusinessToValid = setReq;
        }
        public static void setDocRequests(List<CouponInfo> setReq)
        {
            CouponsToValid = setReq;
        }
       



        public static void addBusinessReq(Business aroma)
        {
            if (BusinessToValid == null)
            {
                BusinessToValid = new List<Business>();
            }
            BusinessToValid.Add(aroma);
        }
        public static void addCouponReq(CouponInfo cou)
        {
            if (CouponsToValid == null)
            {
                CouponsToValid = new List<CouponInfo>();
            }
            CouponsToValid.Add(cou);
        }
        public override string ToString()
        {
            if (this == null)
            {
                return " ";
            }
            else
            {
                return "Admin's his name is: " + this.getFirstName() + " " + this.getLastName() + "\n" + " email: " + this.email + "\n" + " Phone number: " + "0" + this.PhoneNumber;
            }

        }
    }
}
