﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRLG
{
    public class BusinessManager:User
    {

        public List<Business> bus;

    
        
        public BusinessManager(User_account usAcc,String firstName, String lastName, String phoneNumber, String email)
            : base(usAcc,firstName, lastName, phoneNumber, email)
        {
            bus = new List<Business>();
            

        }
        public BusinessManager(String userName, String firstName, String lastName, String phoneNumber, String email)
            :base(userName,firstName,lastName,phoneNumber,email)
        {
            bus = new List<Business>();
        }

      
        public void addBus(Business bus)
        {
            this.bus.Add(bus);
        }
        public void deleteBus(Business bus)
        {
            this.bus.Remove(bus);
        }
        public List<Business> getBusiness()
        {
            return this.bus;
        }
        
        public override string ToString()
        {
            if (this == null)
            {
                return " ";
            }
            else
            {
                return " Business's manager name is: " + this.getFirstName() + " " + this.getLastName() + "\n" + " email: " + this.email + "\n" + " Phone number: " + "0" + this.PhoneNumber + "\n";
            }

        }
    }
}
