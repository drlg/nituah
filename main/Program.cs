﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace main
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection con = new SqlConnection("Server=.\\SQLEXPRESS;Database=DRLG_coupon");
            con.Open();
            SqlCommand com = new SqlCommand("SELECT FirstName, LastName FROM DRLG_User");
            SqlDataReader reader = com.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine("{0}, {1}", reader.GetString(0), reader.GetString(1));
            }

            reader.Close();
            con.Close();
        }
    }
}
